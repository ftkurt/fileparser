# FileParser
File parser and converter for ini, csv, xml and json files
This parser converts all 4 filetypes to JSON (JavaScript Object Notation) format and then converts to the target notation.

# Usage
See demo [here](http://ftkurt.github.io/FileParser/demo/).



HTML
-----
```html

<html>
<head>
...
<script src="test.js"></script>
</head>
<body>
...
<div class="testContainer"></div>
</body>
</html>
```
Options
-------
```js
var iniOpts = {
		type:"ini",
		objectDelimiter:{start:"[",end:"]"},
		valueDelimiter:"=",
		lineEndDelimiter:"\r\n"
	};
var csvOpts = {
		type:"csv",
		valueDelimiter:",",
		lineEndDelimiter:"\r"
	};
var xmlOpts = {
		type:"xml",
	};
var jsonOpts = {
	type:"json"
	}
```

Using by file dialog
--------------------
```js
var options = iniOpts;
var myparser = new parser(options);
myparser.showFileDialog($(".testContainer"));
```

Parsing and Converting with File Handle
------------------------
```js
var myparser = new parser(options);
var json = myparser.parseFile(fileHandle);
myparser.setOptions(xmlOpts);
var xmlText = myparser.convert(json);
```

Parsing and Converting with File Text
------------------------
```js
var myparser = new parser(options);
var json = myparser.parse(fileText);
myparser.setOptions(xmlOpts);
var xmlText = myparser.convert(json);
```
